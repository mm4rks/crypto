#!/bin/env python3
import argparse
import sys
from collections import Counter, defaultdict
from pathlib import Path
from string import ascii_uppercase
from typing import Any, Dict, Iterable

import matplotlib.pyplot as plt

ENGLISH_LETTER_FREQ = {
    "E": 0.1270,
    "T": 0.0906,
    "A": 0.0817,
    "O": 0.0751,
    "I": 0.0697,
    "N": 0.0675,
    "S": 0.0633,
    "H": 0.0609,
    "R": 0.0599,
    "D": 0.0425,
    "L": 0.0403,
    "C": 0.0278,
    "U": 0.0276,
    "M": 0.0241,
    "W": 0.0236,
    "F": 0.0223,
    "G": 0.0202,
    "Y": 0.0197,
    "P": 0.0193,
    "B": 0.0129,
    "V": 0.0098,
    "K": 0.0077,
    "J": 0.0015,
    "X": 0.0015,
    "Q": 0.0010,
    "Z": 0.0007,
}


def get_args():
    """Parse command line arguments"""
    parser = argparse.ArgumentParser(description="Break Vigenère cipher")
    parser.add_argument("file", type=Path, help="Input file")
    return parser.parse_args()


def get_char_frequency(text: str) -> Dict[str, float]:
    counter = Counter(text)
    total = sum(counter.values())
    frequencies = {k: v / total for k, v in counter.items()}
    return defaultdict(float, frequencies)


def plot_histogram(frequencies: dict):
    plt.bar(*zip(*keys_sorted_items(frequencies)))
    plt.show()


def keys_sorted_items(dictionary: Dict[str, Any]):
    return sorted(dictionary.items(), key=lambda item: ord(item[0]))


def sum_of_squares(values: Iterable):
    return sum(v ** 2 for v in values)


def strip_text(text: str) -> str:
    text = text.upper()
    text = "".join(ch for ch in text if ch in ascii_uppercase)
    return text


def rotate(l: list):
    return l[1:] + l[:1]


def get_most_likely_shifts(shift_cipher: str, target_score: float = 0.065):
    """for a given shift_cipher find the most likely shift by matching sum of squares closey to target_score"""
    frequencies = get_char_frequency(shift_cipher)  # dictionary {letter: frequency}
    sorted_cipher_freqs = [v for _, v in keys_sorted_items(frequencies)]
    sorted_english_freqs = [v for _, v in keys_sorted_items(ENGLISH_LETTER_FREQ)]
    candidates = {}
    for i in range(26):
        score = sum(c * r for c, r in zip(sorted_cipher_freqs, sorted_english_freqs))
        candidates[ascii_uppercase[i]] = abs(score - target_score)
        sorted_cipher_freqs = rotate(sorted_cipher_freqs)
    # best 3 candidates
    candidates_sorted_by_score = sorted(candidates.items(), key=lambda item: item[1])
    print(candidates_sorted_by_score[:3])


def break_key(cipher_text: str, key_length: int):
    for i in range(key_length):
        print("=== key letter:", i)
        shift_cipher = cipher_text[i::key_length] + ascii_uppercase
        get_most_likely_shifts(shift_cipher)


def main(arguments):
    cipher_text = arguments.file.read_text()
    cipher_text = strip_text(cipher_text)
    target_score = 0.065
    for tau in range(1, 11):
        frequencies = get_char_frequency(cipher_text[::tau])
        score = sum_of_squares(frequencies.values())
        print(f"{tau}:\t{score:.5f} ({abs(target_score-score):.4f})")
    key_length = 6  # argmin from above
    break_key(cipher_text, key_length)
    return 0


if __name__ == "__main__":
    arguments = get_args()
    sys.exit(main(arguments))
